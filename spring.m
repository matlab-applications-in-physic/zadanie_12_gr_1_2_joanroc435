% notice: program written in MATLAB v 2019b

% Joanna Rocznik
% Technical Physics, Silesian University of Technology
% Gliwice, 2020

% please, credit the copied chunks of code
% but please, do n o t copy mindlessly
% this code is entirely my work, so do have some decency
% if you're planning on stealing it - karma will find you and do its magic

% created with the invaluable help of Julia Pluta

clear;

% 1

r = 0.002;		% radius of the wire (m)
R = 2 * 0.0254;		% radius od the coil (m)
m = 0.01;		% mass of the object (kg)
tau = 2;		% the time suppression constant (s)
g = 9.80665;			% gravitational acceleration (m/s^2)
G = 80E9;		% shear modulus (Pa)
N = 2:1:50;		% list of the number of coils in the spring
f = 0:0.5:500;		% frequency

Fmax = m * g;
a = Fmax/m; 		% alpha
b = 1/(2 * tau);		% beta

%______________________________________________________________________________

% 1
% I'm introducing all the variables neccessary in the calculations below 
%	- including the constants, lists (N and f) and the calculated values
%	(Fmax, a and b)

%______________________________________________________________________________

% 2

for i = 1:size(N, 2);
	k = (G * r^4)/(R^3 * N(i) * 4);
	omega = sqrt(k/m);
	for j = 1:size(f, 2);
		A(j, i) = a/(sqrt((omega^2 - f(j)^2)^2) + (4 * b^2 *f(j)^2));
	end
	[x(i), y] = max(A(:, i));
	omega0(i) = f(y)/2/pi;

	subplot(2, 1, 1);
	plot((f/2/pi), A)
end

xlim([0 80]);
xlabel('Frequency (Hz)');
ylabel('A (m)');

%______________________________________________________________________________

% 2
% In the first part of the loop I'm calculating the values of the spring
%	constant (k) and resonant frequency (omega), both depending on the size
%	of the matrix N.
% Size of the matrix N is a 1x2 matrix - however, I'm only interested in the
%	latter component describing the number of columns (49) - thus the
%	indication: size(N, 2).
% In the second part of the loop - the loop within it - I'm calculating the
%	value of the maximum amplitude (A), which is dependant on both the size of
%	N and the size of the f matrix (the rule here is the same as described above,
%	with the N matrix serving as an example).
% Outside the second loop I'm looking for the maximum value of A in the i-th
%	column and writing it into a new matrix, where x(i) is the value of 
%	A and y is the "place" in the matrix where the maximum value was found.
% Based on that information I'm calculating the frequency "omega".
% Finally, I'm creating a A(omega0) graph showing the peak vaues of A and the
%	corespondig frequencies.
% Outside of all the loops I'm naming the axes and setting the limit on the OX
%	axis

%______________________________________________________________________________

% 3

subplot(2, 1, 2);
plot(N, x);

xlim([0 55]);
xlabel('N');
ylabel('A_{max} (m)');

%______________________________________________________________________________

% 3
% I'm creating the second graph showing how the maximum amplitude changes
%	depending on the number of coils in the spring (N)

%______________________________________________________________________________

% 4

saveas(gcf, 'resonance.pdf');

fileId = fopen('resonance_analysis_results.dat', 'w');

fprintf(fileId, 'The radius of the coil is: R = %.4f m; \nThe radius of the wire is: r = %.3f m; \nThe mass of the attached object is: m = %.2f kg; \nThe time suppression constant is: tau = %.0f s; \nThe Earth_s gravitational acceleration is: g = %.3f m/s^2; \nThe shear modulus is: G = %d Pa \n\n\n', R, r, m, tau, g, G);

for i = 1:size(N, 2);
	fprintf(fileId, 'The number of coils in the spring is: N = %.0f; \nThe maximum amplitude is: A = %.4f m; \nThe value of the omega_0 frequency is: omega_0 = %.4f Hz \n\n\n', N(i), x(i), omega0(i));
end

fclose(fileId);

%______________________________________________________________________________

% 4
% I'm saving both graphs into a .pdf file.
% Then, I'm creating a .dat file into which I'm writing the collected data